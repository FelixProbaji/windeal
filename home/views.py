from django.shortcuts import render

# TODO: Va creati o clasa HomeTemplateView(TemplateView): template_name(html)
# TODO: Definiti un url pentru aceasta clasa din views.py
# TODO: Adaugati un format de meniu pe care vreti sa l aveti in aplicatie(Bootstrap)->base.html

# Create your views here.
from django.views.generic import TemplateView


class HomeTemplateView(TemplateView):
    template_name = 'home/home.html'
