from options.models import Option


def get_all_categories(request):
    categories = Option.objects.all()
    return {'all_categories': categories}
