from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput, EmailInput

from userextend.models import UserExtend


class UserForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['first_name', 'last_name', 'username', 'age',
                  'birthday', 'email']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Please enter your username', 'class': 'form-control'}),
            'age': TextInput(attrs={'placeholder': 'Please enter your age', 'class': 'form-control'}),
            'birthday': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter your e-mail', 'class': 'form-control'}),
        }
