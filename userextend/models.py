from django.contrib.auth.models import User
from django.db import models


class UserExtend(User):
    age = models.IntegerField()
    birthday = models.DateField(null=True)
    active = models.BooleanField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
