from django.urls import path

from userextend import views

urlpatterns = [
    path('create_new_user/', views.UserCreateView.as_view(), name='create-new-user'),
]
