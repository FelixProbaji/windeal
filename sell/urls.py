from django.urls import path

from sell import views

urlpatterns = [
    path('list_own_products/<int:pk>/', views.list_own_products, name='list-own-products'),
]
