from django.shortcuts import render
from product.models import Product


def list_own_products(request, pk):
    products = Product.objects.filter(user_id=pk)
    context = {'own_products': products}
    return render(request, 'sell/list_own_products.html', context)
