from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from product import views

urlpatterns = [
    path('create_product/', views.ProductCreateView.as_view(), name='create-product'),
    path('list_of_products/', views.ProductLIstView.as_view(), name='list-of-products'),
    path('product_details/<int:pk>/', views.ProductDetailView.as_view(), name='product-details'),
    path('product_update/<int:pk>/', views.ProductUpdateView.as_view(), name='product-update'),
    path('product_delete/<int:pk>/', views.ProductDeleteView.as_view(), name='product-delete'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)