from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView

from product.forms import ProductForm
from product.models import Product


class ProductCreateView(LoginRequiredMixin, CreateView):
    template_name = 'product/create_product.html'
    model = Product
    success_url = reverse_lazy('create-product')
    form_class = ProductForm

    def form_valid(self, form):
        if self.request.method == 'POST':
            form = ProductForm(self.request.POST, self.request.FILES)
            if form.is_valid():
                form.save()
                return redirect('list-of-all-products')
        else:
            form = ProductForm
        return redirect('create-product')


class ProductLIstView(ListView):
    template_name = 'product/list_of_products.html'
    model = Product


class ProductDetailView(LoginRequiredMixin, DetailView):
    template_name = 'product/product_details.html'
    model = Product


class ProductUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'product/product_update.html'
    model = Product
    success_url = reverse_lazy('list-of-own-products')
    form_class = ProductForm

    def get_success_url(self):
        return reverse('list-own-products', args=[str(self.request.user.id)])
    

class ProductDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'product/product_delete.html'
    model = Product

    def get_success_url(self):
        return reverse('list-own-products', args=[str(self.request.user.id)])
