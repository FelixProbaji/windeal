from django import forms
from django.forms import TextInput, EmailInput, Select, CheckboxInput

from product.models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['product_name', 'details', 'contact_name', 'contact_email',
                  'contact_phone', 'user', 'location', 'option', 'price', 'negotiable', 'new', 'image']
        widgets = {
            'product_name': TextInput(attrs={'placeholder': 'Product name', 'class': 'form-control'}),
            'details': TextInput(attrs={'placeholder': 'Add details', 'class': 'form-control'}),
            'contact_name': TextInput(attrs={'placeholder': 'Contact name', 'class': 'form-control'}),
            'contact_email': TextInput(attrs={'placeholder': 'Contact email', 'class': 'form-control'}),
            'contact_phone': TextInput(attrs={'placeholder': 'Contact phone', 'class': 'form-control'}),
            'location': Select(attrs={'placeholder': 'Please enter your location',
                                      'class': 'form-control'}),
            'user': Select(attrs={'placeholder': 'Please enter your location',
                                      'class': 'form-control'}),
            'option': Select(attrs={'placeholder': 'Please enter your location',
                                      'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Price', 'class': 'form-control'}),
        }


