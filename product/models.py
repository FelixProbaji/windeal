from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from category.models import Category
from options.models import Option
from userextend.models import UserExtend


class Product(models.Model):
    choices_location = (('Targu-Jiu', 'Targu Jiu'), ('Bucuresti', 'Bucuresti'))
    product_name = models.CharField(max_length=50)
    details = models.CharField(max_length=200)
    contact_name = models.CharField(max_length=30)
    contact_email = models.EmailField()
    contact_phone = models.CharField(max_length=20)
    active = models.BooleanField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=100, choices=choices_location, null=True)
    option = models.ForeignKey(Option, on_delete=models.CASCADE, null=True)
    price = models.IntegerField(default=0)
    negotiable = models.BooleanField(default=1)
    new = models.BooleanField(default=1)
    image = models.FileField(upload_to='images', null=True)

    def __str__(self):
        return self.product_name
