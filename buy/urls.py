from django.urls import path

from buy import views

urlpatterns =[
    path('list_of_all_products/', views.AllProductsListView.as_view(), name='list-of-all-products')
]