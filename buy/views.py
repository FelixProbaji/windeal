from django.shortcuts import render
from django.views.generic import ListView

from product.models import Product


class AllProductsListView(ListView):
    template_name = 'buy/list_of_all_products.html'
    model = Product
    context_object_name = 'all_products'
